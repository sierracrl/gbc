\documentclass[aspectratio=169]{beamer}

 \usepackage{beamerthemesplit} %// Activate for custom appearance
 \usepackage{multicol}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage{graphicx}
\usepackage{chemformula}
\usepackage{pdfpages}

\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\deriv}[1]{\frac{d}{d#1}}
\newcommand{\intl}{\int\limits}
\def\tens#1{\ensuremath{\mathbf{#1}}}


%\usetheme{Luebeck}
\usetheme[compress]{MPIM}
 \usecolortheme{orchid}

\title[Carbon-Climate Feedbacks]{Carbon-Climate Feedbacks and Global Carbon Budget 2020}
\author[Sierra, C.A.]{Carlos A. Sierra}
\institute{Max Planck Institute for Biogeochemistry}
\date{May 6, 2021}

\usefonttheme{serif}

\begin{document}

%%%%%%%%%%%%%%
\frame{\titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Assigned reading}
\centering
\includegraphics[scale=0.5, page=1]{../Reading/Torn2006.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Outline}
\begin{itemize}
\item Carbon-Climate Feedbacks
\item International Climate Framework
\item Global Carbon Budget 2020
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Feedback loops}
\centering
\includegraphics[scale=1.5]{Figures/acoustic_feedback.jpg}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Positive and Negative Feedback}
\centering
\includegraphics[scale=0.3]{Figures/positive_negative_feedback.png}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Forcing, feedbacks, and responses}
\centering
\includegraphics[scale=0.4, page=2]{feedback.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
%\frametitle{Example}
\centering
\includegraphics[scale=0.55, page=4, trim={0 0 0 2.5cm}, clip]{feedback.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Feedback factor and gain}
The feedback factor $f$ is defined as the proportion by which the change in surface air temperature is modified by feedbacks
$$
\Delta T_{eq} = f \cdot \Delta T_0
$$

where $\Delta T_{eq}$ is the equilibrium change in mean global surface temperature, and $\Delta T_0$ is the change in mean surface air temperature that would be required to restore equilibrium without feedbacks. 
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Additive nature of feedbacks}
The equilibrium temperature change is the sum of the temperature change without feedbacks and the temperature change due to feedbacks
$$
\Delta T_{eq} = \Delta T_0 + \Delta T_{\mathrm{feedbacks}}
$$

The temperature change due to different feedbacks can also be additive
$$
\Delta T_{\mathrm{feedbacks}} = \sum_i \Delta T_i
$$
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The system gain}
The system gain is defined as the ratio of the net feedback portion of the temperature change to the total temperature change
$$
g = \frac{\Delta T_{\mathrm{feedbacks}}}{\Delta T_{eq}}
$$

The relation between feedback factor and gain is:
$$
f = \frac{1}{1 -g}
$$
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Calculation of the gain for carbon-climate feedbacks}
$$
g_{\mathrm{CO2}} = \left( \frac{\partial T}{\partial C} \right) \left( \frac{\partial C}{\partial T} \right)
$$

where,
\begin{itemize}
\item $\frac{\partial T}{\partial C}$ is the climate sensitivity; i.e. the change in temperature due to changes in atmospheric CO$_2$ \\
\item $\frac{\partial C}{\partial T} $ is the change in CO$_2$ due to increase in temperature.
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Climate sensitivity from a General Circulation Model (GCM)}
\centering
\includegraphics[scale=0.45, page=6, trim={0 0 0 2.5cm}, clip]{feedback.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{CO$_2$ response from interglacial cycles}
\centering
\includegraphics[scale=0.45, page=8, trim={0 0 0 2.5cm}, clip]{feedback.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Feedback gain due to carbon-climate feedbacks}
$$
g_{\mathrm{CO2}} = \left( \frac{\partial T}{\partial C} \right) \left( \frac{\partial C}{\partial T} \right) = \frac{1.2}{275} \cdot \frac{14.6}{1} = 0.064 
$$

Then,
$$
f_{\mathrm{CO2}} = \frac{1}{1-g_{\mathrm{CO2}}} = \frac{1}{0.936} = 1.068
$$

The CO$_2$ feedback increases the base temperature change by about 7\%

}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nonlinear effect of gains on temperature change}
\centering
\includegraphics[scale=0.4]{Figures/feedbackGain.pdf} \\ \vspace{1em}
\tiny Torn \& Harte (2006, GRL)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Multiple feedbacks}
\centering
\includegraphics[scale=0.4]{Figures/multiple_feedbacks.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Important carbon-climate feedbacks}
\begin{itemize}
\item Soil carbon decomposition
\item Arctic methane release
\item Thawing permafrost
\item Methane clathrates
\item Rain forests dieback
\item Forest fires
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon and climate policy}
\centering
\includegraphics[page=2, scale=0.4]{Figures/SLU_Klimatdialog.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The United Nations Framework Convention on Climate Change}
\centering
\includegraphics[page=12, scale=0.33, trim={0 2cm 0 4.2cm}, clip]{Figures/unfccc_annual_report_2019.pdf}
\includegraphics[page=13, scale=0.33, trim={0 2cm 0 4.2cm}, clip]{Figures/unfccc_annual_report_2019.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Intergovernmental Panel on Climate Change}
\centering
\includegraphics[scale=0.4]{Figures/ipcc_structure.png}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{IPCC Reports}
\centering
\includegraphics[scale=0.5]{Figures/IPCC-Reports.jpg}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{IPCC Reports}
\centering
\includegraphics[scale=0.55]{Figures/Fig-2.2_rev1-01-2.png}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The Global Carbon Project}
\centering
\includegraphics[scale=0.55]{Figures/Global-Carbon-Project.jpg}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=1, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=14, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=15, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=16, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=19, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=20, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=21, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=26, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=27, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=82, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=79, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=80, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=30, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=36, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=38, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=41, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\includegraphics[page=70, scale=0.45]{Figures/GCP_CarbonBudget_2020.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Take-home messages}
\begin{itemize}
\item Positive and negative carbon-climate feedbacks can considerably modify predictions of future climate change
\item Carbon-Climate feedbacks are highly uncertain and may lead to surprising outcomes
\item Carbon emissions are still very large despite the COVID-19 pandemic
\item Although emission trends have changed, atmospheric CO$_2$ continues the same trend
\item Emissions can be analyzed with respect to emitters, energy source, trade, or economic activity. Climate policy must take these different aspects into consideration.
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
