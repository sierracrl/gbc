\documentclass[aspectratio=169]{beamer}

 \usepackage{beamerthemesplit} %// Activate for custom appearance
 \usepackage{multicol}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage{graphicx}
%\usepackage{minted}

\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\deriv}[1]{\frac{d}{d#1}}
\newcommand{\intl}{\int\limits}

%\usetheme{Luebeck}
\usetheme[compress]{MPIM}
 \usecolortheme{orchid}

\title[The global C cycle]{The Global Carbon Cycle (Atmosphere and Oceans)}
\author[Sierra, C.A.]{Carlos A. Sierra}
\institute{Max Planck Institute for Biogeochemistry}
\date{April 11, 2024}

\usefonttheme{serif}

\begin{document}

%%%%%%%%%%%%%%
\frame{\titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Outline}
%\begin{itemize}
%\item Introduction to the global C cycle, the greenhouse effect, and global carbon models
%\item Modification to the global C cycle
%%\item The global carbon budget and society
%\end{itemize}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Assigned reading}
\centering
%\includegraphics[page=1, scale=0.5, trim={0, 0, 0, 10cm}, clip]{../../../Documents/Papers/3601-3800/3602.pdf}
\includegraphics[scale=0.2]{Graven2016} \\ \vspace{1em} \small
Graven, H.~D. (2016). The carbon cycle in a changing climate. {\em Physics Today}, 69(11):48--54.
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Natural carbon cycle}
\centering
\includegraphics[scale=0.35]{naturalCcycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Chemical forms of carbon}
\centering
\includegraphics[scale=0.35]{../lecture7/Figures/chemicalForms.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global C stocks}
\centering
\includegraphics[scale=0.35]{Cstocks}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The recent C cycle (2013-2022)}
\centering
%\includegraphics[scale=0.4]{CCycle} \\
\includegraphics[page=6, scale=0.6, trim={2cm, 15cm, 2cm, 2cm}, clip]{essd-15-5301-2023.pdf} \\
\tiny Friedlingstein et al. (Global Carbon Budget 2023) \\
\url{https://essd.copernicus.org/articles/15/5301/2023/essd-15-5301-2023.pdf}
%\tiny Intergovernmental Panel on Climate Change (2013, 5th Assessment Report)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Atmospheric CO$_2$ over the past 60 million years}
\includegraphics[scale=0.55, page=11, trim={2cm, 4cm, 2cm, 12cm}, clip]{IPCC_AR6_WGI_Chapter05.pdf} \\ \vspace{0.2cm}
\tiny IPCC (2021, 6th Assessment Report) \\
\url{https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Chapter05.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Recent increase of C dioxide in the atmosphere}
\centering
\includegraphics[scale=0.4]{mlo_record_2024.png} 
%\includegraphics[scale=0.28]{globalTemp} 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon dioxide and climate change}
\centering
\includegraphics[scale=0.2, trim={34cm, 0, 0, 10cm}, clip]{IPCC_AR6_WGI_SPM_Figure_1.png} \\
\tiny IPCC (2021, AR6)
%\includegraphics[scale=0.28]{mlo_full_record.pdf} 
%\includegraphics[scale=0.28]{globalTemp} 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Contribution of GHG emissions to atmospheric warming}
\centering
\begin{multicols}{2}
\includegraphics[scale=0.25]{radiativeFlux.pdf}

\vspace{1em}
\tiny Zhong \& Haigh (2013, Weather 68: 100)

\columnbreak
\includegraphics[scale=0.3]{Rodhe1.pdf} \\
\includegraphics[scale=0.3]{Rodhe2.pdf} \\
\vspace{1em}
Rodhe (1990, Science 248: 1217)

\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Carbon emissions and global temperature change}
%\centering
%\includegraphics[scale=0.6]{TCRE} \\
%\tiny IPCC (2018)
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Seasonal cycle of atmospheric CO$_2$}
\centering
\includegraphics[scale=0.35]{seasonalCycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Earth's biosphere: oceanic and terrestrial components}
\centering
\includegraphics[scale=0.23]{global_biosphere} \\ \tiny \vspace{1em} {\it \textcopyright SeaWiFS project, NASA}
}
%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Atmospheric CO$_2$ and temperatures in Jena}
\centering
\url{https://www.bgc-jena.mpg.de/~martin.heimann/weather/weather_co2/} \\ \vspace{2em}
\url{https://www.bgc-jena.mpg.de/~martin.heimann/weather/weather_temperature2/}
%Go to Weather Station $\to$ Time Series of CO$_2$ Concentration in Jena
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Atmospheric CO$_2$ and COVID-19}
%\centering
%\includegraphics[scale=0.16]{covid1}
%\includegraphics[scale=0.17]{covid2} \\
%\tiny \url{https://www.icos-cp.eu/event/933}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Atmospheric CO$_2$ records from other stations}
\begin{multicols}{2} 
\includegraphics[scale=0.3]{co2_stations}

\columnbreak
\includegraphics[scale=0.26]{co2_sta_records}
\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{3D animation of CO$_2$ transport}
\centering
\url{https://youtu.be/syU1rRCp7E8}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Seasonal cycle of atmospheric CO$_2$}
%\centering
%\includegraphics[scale=0.35]{Graven2016_1}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{What is the origin of carbon in the atmosphere?}
\emph{How do we know that the increase in atmospheric CO$_2$ is due to the combustion from fossil fuels?}

}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Global anthropogenic CO$_2$ emissions}
\includegraphics[scale=0.7, page=16, trim={1.8cm, 18.7cm, 2cm, 1.8cm}, clip]{IPCC_AR6_WGI_Chapter05.pdf} \\ \vspace{0.2cm}
\tiny IPCC (2021, 6th Assessment Report) \\
\url{https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Chapter05.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{isotopes}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{isotopeRatio}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{C13cycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.35]{SuessEffect13C}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{C14production}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{C14cycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{SuessEffectC14}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.5]{bomb}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.9]{HuaSeries}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.6]{GravenPNAS}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Airborne fraction: proportion of anthropogenic emissions that remains}
\centering
\includegraphics[scale=0.7, page=18, trim={1.8cm, 3cm, 2cm, 16cm}, clip]{IPCC_AR6_WGI_Chapter05.pdf} \\ \vspace{0.2cm}
\tiny IPCC (2021, 6th Assessment Report) \\
\url{https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Chapter05.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Ocean and land carbon sinks}
\centering
\includegraphics[scale=0.8, page=60, trim={1.8cm, 3cm, 2cm, 18cm}, clip]{IPCC_AR6_WGI_Chapter05.pdf} \\ \vspace{0.2cm}
\tiny IPCC (2021, 6th Assessment Report) \\
\url{https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Chapter05.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Strength of natural sinks and feedbacks}
\centering
%\includegraphics[scale=0.32]{gcp2}
\includegraphics[page=47, scale=0.4]{GCP2019.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Saturation of ocean and land carbon sinks}
\centering
\includegraphics[scale=1, page=62, trim={3cm, 21.1cm, 2cm, 1.8cm}, clip]{IPCC_AR6_WGI_Chapter05.pdf} \\ \vspace{0.2cm}
\tiny IPCC (2021, 6th Assessment Report) \\
\url{https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Chapter05.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{TCRE: transient climate response to cumulative emissions}
\centering
\includegraphics[scale=0.8, page=78, trim={2cm, 18cm, 2cm, 1.8cm}, clip]{IPCC_AR6_WGI_Chapter05.pdf} \\ \vspace{0.2cm}
\tiny IPCC (2021, 6th Assessment Report) \\
\url{https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Chapter05.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Remaining carbon budget}
\centering
\includegraphics[scale=0.65, page=106, trim={2cm, 13cm, 2cm, 5cm}, clip]{IPCC_AR6_WGI_Chapter05.pdf} \\ \vspace{0.2cm}
\tiny IPCC (2021, 6th Assessment Report) \\
\url{https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Chapter05.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Fossil fuel emissions}
%\centering
%\includegraphics[page=5, scale=0.5, trim={1cm, 0, 1cm, 4.5cm}, clip]{GCP_CarbonBudget_2022.pdf}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Fossil fuel emissions}
%\centering
%\includegraphics[page=6, scale=0.5, trim={1cm, 0, 1cm, 4.5cm}, clip]{GCP_CarbonBudget_2022.pdf}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Fossil fuel emissions}
%\centering
%\includegraphics[page=7, scale=0.5, trim={1cm, 0, 1cm, 5cm}, clip]{GCP_CarbonBudget_2022.pdf}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Fossil fuel emissions}
%\centering
%\includegraphics[page=13, scale=0.5, trim={1cm, 0, 1cm, 4.5cm}, clip]{GCP_CarbonBudget_2022.pdf}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Fossil fuel emissions}
%\centering
%\includegraphics[page=16, scale=0.5, trim={1cm, 0, 1cm, 4.5cm}, clip]{GCP_CarbonBudget_2022.pdf}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Uptake of C from natural sinks}
%\centering
%%\includegraphics[scale=0.3]{gcp1}
%\includegraphics[page=45, scale=0.4]{GCP2019.pdf}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Possible reasons for the enhanced terrestrial C sink}
\centering
\includegraphics[scale=0.26]{regrowth}
}
%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Possible reasons for the enhanced terrestrial C sink}
\centering
\includegraphics[scale=0.25]{fertilization}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Possible trajectories predicted by ESMs}
\includegraphics[scale=1.1, trim={7cm 0 0 6cm}, clip]{Fried2} \\
\vspace{1em}
\tiny Friedlingstein et al. (2014)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Understanding processes in the terrestrial biosphere}
\centering
\includegraphics[scale=0.4]{transitTime.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Representation of ecosystem C processes in models}
%\centering
%\includegraphics[scale=0.5]{cModel}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The photosynthesis model of Farquhar et al.}
\centering
\includegraphics[scale=0.5]{Farquhar}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Environmental controls on photosynthesis}
\centering
\includegraphics[scale=0.45]{lightdose}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Allocation of carbon to different vegetation components}
\centering
\includegraphics[scale=0.8, trim={0 9cm 0 0}, clip]{Hartmann} \\
\small A: assimilation, R: respiration, G; growth, NSC: non-structural carbohydrates
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Storage, cycling and release from soils}
\centering
\includegraphics[scale=0.4]{transitTime.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Possible trajectories predicted by ESMs}
\includegraphics[scale=1.1, trim={7cm 0 0 6cm}, clip]{Fried2} \\
\vspace{1em}
\tiny Friedlingstein et al. (2014)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Future trajectories will depend on the balance between photosynthesis and respiration}
\centering
\includegraphics[scale=0.25, trim=20cm 15cm 0cm 0cm, clip]{GPP_Beer}
\includegraphics[scale=0.38]{Hashimoto} \\ 
}
%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The global carbon budget 2019}
\centering
\includegraphics[page=1,scale=0.55]{gcb2019}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=12, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=13, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=14, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=16, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=20, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=30, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=35, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Take home messages (Atmosphere)}
\begin{itemize}
\item The global carbon cycle has been largely modified by anthropogenic activities, particularly the atmospheric compartment
\item The seasonal cycle of atmospheric CO$_2$ is largely controlled by the biosphere
\item Emissions of coal and oil from major global economies are responsible for most of the emissions
\item The land and the oceans take up together more than half of anthropogenic carbon emissions
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Key concepts to remember}
\begin{itemize}
\item Radiative forcing
\item Absolute global warming potential
\item Airborne fraction
\item Transient climate response to cumulative emissions
\item Remaining carbon budget
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%


\end{document}


