\documentclass[aspectratio=169]{beamer}

 \usepackage{beamerthemesplit} %// Activate for custom appearance
 \usepackage{multicol}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage{graphicx}
\usepackage{chemformula}
\usepackage{pdfpages}

\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\deriv}[1]{\frac{d}{d#1}}
\newcommand{\intl}{\int\limits}
\def\tens#1{\ensuremath{\mathbf{#1}}}


%\usetheme{Luebeck}
\usetheme[compress]{MPIM}
 \usecolortheme{orchid}

\title[The Global P Cycle]{The Global Phosphorus Cycle}
\author[Sierra, C.A.]{Carlos A. Sierra}
\institute{Max Planck Institute for Biogeochemistry}
\date{December 11, 2024}

\usefonttheme{serif}

\begin{document}

%%%%%%%%%%%%%%
\frame{\titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Recommended reading}
\centering
\includegraphics[scale=0.25, page=1]{../Reading/Elser}

%Reading/lecture7-P-Cycle.pdf
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global phosphorus balance}
\centering
\includegraphics[scale=0.3]{Figures/Phosphorkreislauf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The Global P Cycle}
%\centering
%\includegraphics[scale=0.35]{Figures/globalPcycle}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Chemical forms of biogeochemical elements}
\centering
\includegraphics[scale=0.35]{Figures/chemicalForms}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Global phosphorus reservoirs}
%\centering
%\includegraphics[scale=0.4]{Figures/Preservoirs}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphorus mineral forms}
\centering
\includegraphics[scale=0.35]{Figures/mineralForms}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Different forms of apatite}
\centering
\includegraphics[scale=0.5]{Figures/apatiteForms}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Dissolved inorganic forms of phosphorus}
\centering
Phosphate, \ch{PO4^3-}, is the fully dissociated anion of phosphoric acid, \ch{H3PO4}:

\vspace{3em}
\ch{H3PO4 <-> H+ + H2PO4^- <-> 2 H+ + HPO4^2- <-> 3 H+ + PO4^3-} 

\includegraphics[scale=0.08]{Figures/phosphate.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphoric acid speciation}
\centering
\includegraphics[scale=0.5]{Figures/phosphoricAcid}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphoric acid speciation in sea water}
\centering
\includegraphics[scale=0.5]{Figures/seaWaterDissolution}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Organic forms of phosphorus}
Phosphorus is a fundamental element for life. It is part of:
\begin{itemize}
\item Nucleic acids: DNA, and RNA
\item Adenosine triphosphate ATP
\item Phospholipids
\item Bones (as apatite, \ch{Ca10(PO4)6(OH)2)}
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphorus in DNA}
\centering
\includegraphics[scale=0.35]{Figures/PDNA}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Adenosine triphosphate as energy carrier}
\centering
\includegraphics[scale=0.65]{Figures/ATP} \\
\vspace{1em}
When ATP is hydrolyzed to form ADP and inorganic phosphate, 30.5 kJ of energy are released.
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phospholipids in cell membranes}
\centering
\includegraphics[scale=0.35]{Figures/phospholipid}
\includegraphics[scale=0.55]{Figures/cellMembrane}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Stoichiometry of important biomolecules}
\centering
\includegraphics[scale=0.43]{Figures/biomoleculeStoichiometry.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Stoichiometry of biological organisms}
\centering
\includegraphics[scale=0.17]{Figures/resourceStoichiometry} \\ \vspace{2em}
\tiny Scholarly Community Encyclopedia
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Constant proportions of elements in oceans (Redfield ratios)}
\centering
\includegraphics[scale=0.25]{Figures/redfield}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphorus transfers from terrestrial to marine systems}
\centering
\includegraphics[scale=0.5]{Figures/Ptransfers}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Uplift and erosion}
\centering
\includegraphics[scale=0.4]{Figures/uplift}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Fate of phosphorus after rock exposure (Walker \& Syers model)}
\centering
\includegraphics[scale=0.4]{Figures/WalkerSyers}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Test of the Walker \& Syers model in Hawaii (Crews et al. 1995)}
\centering
\includegraphics[scale=0.5]{Figures/Crews1}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Test of the Walker \& Syers model in Hawaii (Crews et al. 1995)}
\centering
\includegraphics[scale=0.4]{Figures/Crews2}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{What is occluded phosphorus?}
\centering
\includegraphics[scale=0.8]{Figures/occludedP.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Occluded P: mechanical inclusion of sorbed P in Fe-oxides}
\centering
\includegraphics[scale=0.6]{Figures/FeAloxides}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Bioavailability and occlusion}
\centering
\includegraphics[scale=0.35]{Figures/soilP}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{P mobilization by plants}
\begin{itemize}
\item Acidification of the rhizosphere
\item Release of chelators that complex iron
\item Release of phosphatases that catalyze the hydrolysis of organic P
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Root acidification}
\centering
\includegraphics[scale=1]{Figures/rootAcidification}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Release of chelators that complex metal ions}
\centering
\begin{itemize}
\item Chelators have a strong tendency to bind \ch{Al^3+}, \ch{Fe^2+}, \ch{Ca^2+}, \ch{Mg^2+}, \ch{Mn^2+}, \ch{Zn^2+}, \ch{Cu^2+}
\item They lower the activity of free cations in the soil solution
%\item They extract elements from surfaces by electron transfer
\end{itemize}
\vspace{2em}
\includegraphics[scale=0.4]{Figures/Chelator.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Microbes and roots can release phosphatases}
\centering
\includegraphics[scale=1]{Figures/spohn} \\
\vspace{1em}
\ch{R - OPO3^2- + H2O -> R - OH + H+ + PO4^3-}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphorous is commonly a limiting nutrient for plant growth}
\centering
\includegraphics[page=3, trim={2cm, 13cm, 2cm, 1.5cm}, clip, height=7cm]{articles/Hou2020.pdf} \\
\tiny Huo et al. (2020, Nat Comm)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Cycling of phosphorus in lakes (summer)}
\centering
\includegraphics[scale=0.3]{Figures/lakeDistribution}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Eutrophication due to excess phosphorus}
\centering
\includegraphics[scale=0.4]{Figures/eutrophication}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphorus in the ocean}
\centering
\includegraphics[scale=0.4]{Figures/oceanP}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphate concentrations in the ocean}
\centering
\includegraphics[scale=0.28]{Figures/OceanP_surface.png}
\includegraphics[scale=0.28]{Figures/OceanP_200m.png}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphate concentrations in the ocean}
\centering
\includegraphics[scale=0.28]{Figures/OceanP_1000m.png}
\includegraphics[scale=0.28]{Figures/OceanP_2000m.png}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphate concentrations by ocean basin}
\centering
\includegraphics[scale=0.5, page=65, trim={5cm, 12cm, 5cm, 3cm}, clip]{articles/WorldOceanAtlas2023.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{P inputs to oceans}
\centering
\includegraphics[scale=0.6]{Figures/Pin2Ocean}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Excess P inputs cause ocean eutrophication}
\centering
\includegraphics[scale=0.3]{Figures/oceanEutrophication}
\includegraphics[scale=0.4]{Figures/oceanEutrophication2}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Ocean sedimentation of phosphorus}
\centering
\includegraphics[scale=0.5]{Figures/oceanSedimentation}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Burial of phosphorus in marine sediments}
\centering
\includegraphics[scale=0.3]{Figures/authigenic1}
\includegraphics[scale=0.3]{Figures/authigenic2}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Phosphorus stores in ocean sediments}
%\centering
%\includegraphics[scale=0.27]{Figures/PinSediments}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Ocean sedimentation of phosphorus}
\centering
\includegraphics[scale=0.4]{Figures/PformsSediments}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Transport of P in dust}
\centering
\includegraphics[scale=0.4]{Figures/sahara-dust}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{P fertilization from dust}
\centering
\includegraphics[scale=0.9, page=3, trim={2cm, 12cm, 2cm, 9.6cm}, clip]{articles/Mahowald2008.pdf} \\
\tiny Mahowald et al. (2008, Global Biogeochem Cycles 22)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Terrestrial and marine P processes combined}
\centering
\includegraphics[page=2,scale=0.55, trim={3cm 2.5cm 3cm 3.5cm},clip]{articles/RuttenbergGlobalPCycle.pdf} \\
\tiny Ruttenberg (2003)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global phosphorus balance}
\centering
\includegraphics[scale=0.4]{Figures/globalPbalance}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphate mining}
\centering
\includegraphics[scale=0.08]{Figures/Phosphate_Mine}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global use of phosphorus as fertilizer}
\centering
\includegraphics[scale=0.8]{Figures/Pfertilizer}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Use of P reserves}
\centering
\includegraphics[scale=1.2]{Figures/peakP_2009.jpg}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Phosphate reserves reported by country}
\centering
\includegraphics[scale=0.9, page=6, trim={4cm, 19cm, 4cm, 2cm}, clip]{articles/Walsh2023.pdf} \\
\tiny Walsh et al. (2023, NPJ Sust Agr 2)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{US phosphate reserves, exports, and imports}
\centering
\includegraphics[scale=0.27]{Figures/US_Mined_Phosphate_Rock_1900-2015.png} 
\includegraphics[scale=0.27]{Figures/US_Net_Imports_Phosphate_Rock_1900-2015.png} \\
\tiny USGS (2016)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global phosphate reserves, and mined from reserves}
\centering
\includegraphics[scale=0.55, page=4, trim={5cm, 13cm, 5cm, 2cm}, clip]{articles/Walsh2023.pdf} \\
\tiny Walsh et al. (2023, NPJ Sust Agr 2)
%\includegraphics[scale=0.4]{Figures/Global_phosphate_reserves.jpg} \\
%\tiny USGS (2016)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Fluxes (Pg/yr) and efficiency in P use}
\centering
\includegraphics[scale=0.8, page=4, trim={4cm, 4cm, 4cm, 16cm}, clip]{articles/Elser.pdf} \\
\tiny Elser (2012, Current Opinion in Biotech 23:833)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{From P peak to a Circular Phosphorous Economy}
\centering
\includegraphics[scale=1.2, page=8, trim={3cm, 21cm, 11cm, 2cm}, clip]{articles/Walsh2023.pdf} \\ \vspace{1em}
\tiny Walsh et al. (2023, NPJ Sust Agr 2)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Technologies for P recovery}
\centering
\includegraphics[scale=0.37, page=8, trim={2cm, 7.6cm, 2cm, 1.5cm}, clip]{articles/Jupp2021.pdf} \\ 
\tiny Jupp et al. (2021, Chem Soc Rev 50:87)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Summary and take-home messages}
\begin{itemize}
\item Phosphorus is one of the most important elements for life, it is part of important molecules such as DNA, ATP, and phospholipids.
\item Phosphorus is generally a limiting nutrient for the growth of plant, algae, and phytoplankton.
\item Compared to other elements, there is not much P around, it doesn't travel well, and it is not readily bioavailable. 
\item P use in fertilizers is currently unsustainable and have negative impacts on aquatic ecosystems. 
\item Urgent need to change to a circular P economy.
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}

