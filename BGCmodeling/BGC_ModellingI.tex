\documentclass[aspectratio=169]{beamer}

 \usepackage{beamerthemesplit} %// Activate for custom appearance
 \usepackage{multicol}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage{graphicx}
\usepackage{chemformula}

\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\deriv}[1]{\frac{d}{d#1}}
\newcommand{\intl}{\int\limits}
\def\tens#1{\ensuremath{\mathbf{#1}}}


%\usetheme{Luebeck}
\usetheme[compress]{MPIM}
 \usecolortheme{orchid}

\title[Modelling Global Biogeochemical Cycles]{Modelling Global Biogeochemical Cycles}
\author[Sierra, C.A.]{Carlos A. Sierra}
\institute{Max Planck Institute for Biogeochemistry}
\date{May 16, 2024}

\usefonttheme{serif}

\begin{document}

%%%%%%%%%%%%%%
\frame{\titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Assigned reading}
%\centering
%\includegraphics[scale=0.6]{../lecture1/Jacobson}
%
%Chapter 4: Modeling Biogeochemical Cycles
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Outline}
%\begin{itemize}
%\item Mass balance equations
%\item Matrix representation
%\item Diagnostic times
%\end{itemize}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Earth system models}
\begin{multicols}{2}
\includegraphics[scale=0.5]{Figures/esm}

\columnbreak
\begin{itemize}
\item Movement and transport of energy and matter across Earth's main reservoirs
\item Use for climate change simulations
\item Help us understand connections among Earth system processes
\end{itemize}
\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Biogeochemical elements follow conservation laws}
\begin{itemize}
\item Mass cannot be created or destroyed. The change of mass $M$ over time is the result of inputs minus outputs of mass. 

\begin{equation}
\frac{dM}{dt} = Inputs - Outputs \notag
\end{equation}
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Compartmental theory}
Definitions:
\begin{itemize}
\item Compartment/reservoir/pool: an amount of material defined by certain physical, chemical, or biological characteristics and is kinetically homogeneous. We characterize compartments by their mass.
\item Flux: amount of material transferred from one compartment to another per unit of time, i.e. [mass time$^{-1}$] 
\item Rate: relative speed of change of the mass of a reservoir in units of inverse time, i.e. [time$^{-1}$]
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Steps for modeling biogeochemical cycling}
\begin{itemize}
\item Define the boundaries and structure of the system through a conceptual diagram
\item Define a mathematical model that captures the structure of the conceptual diagram
\item Find the best set of parameters of the model
\item Manipulate the mathematical model to observe its dynamics over time and compute other interesting diagnostics
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Simple example: radioactive decay}
\centering
\includegraphics[scale=0.5]{Figures/radioactiveDecay}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Conceptual model: radioactive decay}
\centering
\includegraphics[scale=0.5]{Figures/radiocarbonModel} \\ \vspace{1cm}

\small
Question: \emph{How much mass remains after x units of time?}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Mathematical model}

$$ 
  \frac{dx}{dt} = Inputs - Outputs
$$

or

$$ 
  \frac{dx}{dt} = Sources - Sinks
$$

}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Conceptual model: radioactive decay}
\centering
\includegraphics[scale=0.4]{Figures/radiocarbonModel} \\ \vspace{1cm}

$$ 
  \frac{dN}{dt} = - \lambda \cdot N
$$

}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Mathematical solution}

Initial value problem (IVP)

$$ 
  \frac{dN}{dt} = - \lambda \cdot N, \quad N(t=0) = N_0
$$

}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Mathematical solution}

Initial value problem

$$ 
  \frac{dN}{dt} = - \lambda \cdot N, \quad N(t=0) = N_0
$$

Solution: 

$$
  N(t) = N_0 \exp(-\lambda \cdot t)
$$
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Parameterization}

Libby half-life for radiocarbon is 5568 years!

$$
  t_{1/2} = 5568 = \frac{\ln 2}{\lambda},
$$

Then,
$$
\lambda = 0.0001244876 \quad \text{years}^{-1}
$$
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Prediction}
\centering
How much radiocarbon would be available after 10,000 years of radioactive decay if the initial amount of $^{14}$C atoms $N_0 = 100$?

\includegraphics[scale=0.4]{Figures/C14remaining} \\

\begin{align}
  N(t=10000) &=100 \exp(\lambda \cdot 10000) \notag \\
  &= 28.8 \notag
\end{align}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{The one pool model}
\begin{multicols}{2}
\includegraphics[scale=0.25]{Figures/oneP.pdf}

\columnbreak

The mass balance equation: \\
\begin{align}
\frac{dx}{dt} &= I - O \notag \\
                    &= I - k \cdot x \notag
\end{align}

\begin{itemize}
\item $I$: External flux from outside the system [mass $\cdot$ time$^{-1}$].
\item $O =k \cdot x$: Flux leaving the system.
\item $k$: rate at which mass leaves the system [time$^{-1}$].
\end{itemize}

\end{multicols}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Analytical solution of the one pool model}
\begin{multicols}{2}
\begin{equation}
x(t) = \frac{I}{k} - \left( \frac{I}{k} - x(0) \right) e^{-k \cdot t} \notag
\end{equation}
With initial condition $x(0) = 0$ Pg, $I=10$ Pg yr$^{-1}$, and $k=0.5$ yr$^{-1}$.

\columnbreak
\includegraphics[scale=0.4]{Figures/xt}
\end{multicols}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Effect of the initial conditions}
\begin{multicols}{2}
\includegraphics[scale=0.4]{Figures/x0t}
\columnbreak

Steady-State:

At steady-state, inputs are equal to outputs ($I = O  = k \cdot x$), and 
\begin{equation}
x = \frac{I}{k} \notag
\end{equation}
\end{multicols}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Turnover time}
At steady-state, the ratio of the stock and the input or output flux is defined as the turnover time $\tau$

\begin{align}
\tau &= \frac{x}{I} = \frac{x}{k \cdot x} \notag \\
\tau &= \frac{1}{k} \notag
\end{align}

Turnover time is usually interpreted as the time it would take to renew all mass in the reservoir, or the time it would take to fill or empty the reservoir.
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Heterogeneous interconnected systems}
\centering
\includegraphics[scale=0.5]{Figures/conceptualModel}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Concept to math: balance equations}
\centering
\includegraphics[scale=0.35]{Figures/balanceModel} \\
\begin{align}
\frac{d \text{SV}_1}{dt} = F0 - F1 \notag \\
\frac{d \text{SV}_2}{dt} = F1 - F2 \notag
\end{align}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Example: a lake ecosystem}
C cycle

\begin{multicols}{2}
\includegraphics[scale=0.3]{Figures/lakeC}
\columnbreak

\includegraphics[scale=0.3]{Figures/lakeCsys}
\end{multicols}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
%\frame{
%\frametitle{The matrix with fluxes among compartments}
%
%$$
%\mathbf{F} = \left( \begin{matrix}
%			-f_{1,1} & f_{1,2} & \dots & f_{1,n} \\
%			f_{2,1} & -f_{2,2} & \dots & f_{2,n} \\
%			\vdots & \vdots & \ddots & \vdots \\
%			f_{n,1} & f_{n,2} & \dots & -f_{n,n}
%		    \end{matrix} \right)
%$$
%}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Mathematical formulation for the fluxes}

The law of mass action:
$$
 \text{Reaction Rate} = k \cdot [A]^\alpha \cdot [B]^\beta
$$

\emph{The rate of the reaction is proportional to a power of the concentrations of all
substances taking part in the reaction}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Reaction order}
\centering
\includegraphics[scale=0.3]{Figures/reactionOrder}

\vspace{1em}
\begin{itemize}
\item Zero order: $k_0$
\item First order: $k_1 \cdot [A]$
\item Second order: $k_2 \cdot [A] \cdot [B]$
\item Third order: $k_3 \cdot [A]^2 \cdot [B]$
\end{itemize}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{First order models are very common}
\begin{multicols}{2}
\includegraphics[scale=0.8]{Figures/twopseries}
\columnbreak

\begin{align}
\frac{dC_1}{dt} &= I - k_1 C_1 \notag \\
\frac{dC_2}{dt} &= \alpha k_1 C_1 - k_2 C_2 \notag
\end{align}
\end{multicols}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Nomenclature of pool models}
\centering
\includegraphics[scale=0.5]{Figures/Pools}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{General matrix representation}

Any model using first-order reaction terms with constant coefficients of the form

\begin{align}
\frac{dx_1}{dt} &= I_1 + \sum \alpha_{1,j} k_j x_j - k_1 x_1 \notag \\
\frac{dx_i}{dt} &= I_i + \sum \alpha_{i,j} k_j x_j - k_i x_i \notag \\
\frac{dx_n}{dt} &= I_n + \sum \alpha_{n,j} k_j x_j - k_n x_n \notag
\end{align}

can be expressed as a system of the form

$$
\frac{d {\bm x}}{dt} = {\bm I} + {\bf A} \cdot {\bm x}
$$
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Analytical solution}

The system
$$
\frac{d {\bm x}}{dt} = {\bm I} + {\bf A} \cdot {\bm x},
$$

with initial conditions ${\bm x}(t=0) = {\bm x}_0$, has solution
$$
 \bm{x}(t) =e^{{\bf A} \cdot (t-t_0)}  \bm{x}_0 + \left( \int_{t_0}^{t} e^{{\bf A} \cdot (t- \tau)} d\tau \right) \bm{I}.
$$

At steady-state:

$$
{\bm x_{ss}} =  -{\bf A}^{-1} \cdot {\bm I}
$$

}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Turnover time for heterogenous systems at steady-state}
The ratio of all stocks over all inputs is defined as the turnover time of the system
\begin{equation}
\tau = \frac{\sum {\bm x}_{ss}}{\sum {\bm I}} \notag
\end{equation}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Numerical solutions}

Calculate the solution of the IVP taking advantage of the know values of the derivatives and the initial value. For the IVP
$$
x'(t) = f(t,x(t)), \qquad x(t_0)=x_0,
$$

an approximation to the solution is

$$
x(t+h) \approx x(t) + hf(t,x(t)). 
$$

This leads to a recursion of the form

$$
x_{n+1} = x_n + hf(t_n,x_n).
$$
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Run multiple-pool models numerically}
\begin{multicols}{2}
\includegraphics[scale=0.6]{Figures/ThreepModels}

\columnbreak

\small
\begin{itemize}
\item Parallel: no exchange of matter among pools

\item Series: Progressive transfer of matter among pools

\item Feedback: Multiple exchange of matter among pools. 
\end{itemize}
\end{multicols}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Mathematical form}

\begin{itemize} \small
\item Parallel: \begin{equation} \label{eq:ThreepParallel}
\frac{d {\bm x}(t)}{dt} = I \left( \begin{array}{c} \gamma_1 \\ \gamma_2 \\ 1-\gamma_1-\gamma_2 \end{array} \right) +
\left( \begin{array}{ccc}
-k_1 & 0 & 0 \\
0 & -k_2 & 0 \\
0 & 0 & -k_3
\end{array} \right)
\left( \begin{array}{c} x_1 \\ x_2 \\ x_3 \end{array} \right) \notag
\end{equation}

\item Series: 
\begin{equation} \label{eq:ThreepSeries}
\frac{d {\bm x}(t)}{dt} = I \left( \begin{array}{c} 1 \\ 0 \\ 0 \end{array} \right) +
\left( \begin{array}{ccc}
-k_1 & 0 & 0 \\
a_{21} & -k_2 & 0 \\
0 & a_{32} & -k_3
\end{array} \right)
\left( \begin{array}{c} x_1 \\ x_2 \\ x_3 \end{array} \right) \notag
\end{equation}

\item Feedback:
\begin{equation} \label{eq:ThreepFeedback}
\frac{d {\bm x}(t)}{dt} = I \left( \begin{array}{c} 1 \\ 0 \\ 0 \end{array} \right) +
\left( \begin{array}{ccc}
-k_1 & a_{12} & 0 \\
a_{21} & -k_2 & a_{23} \\
0 & a_{32} & -k_3
\end{array} \right)
\left( \begin{array}{c} x_1 \\ x_2 \\ x_3 \end{array} \right) \notag
\end{equation}


\end{itemize}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
%\frame{
%\frametitle{Effect of model structure: same inputs and rates}
%\centering
%\includegraphics[scale=0.4]{Figures/xt_threep}
%}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\section{Ages and transit times}
\frame{
\frametitle{Model properties: Ages and transit times}
\centering
\includegraphics[scale=0.2]{Figures/AgeTransitTime}

\footnotesize
\begin{itemize}
\item {\it System age} is a random variable that describes the age of particles or molecules within a system since the time of entry.
\item {\it Pool age} is a random variable that describes the age of particles or molecules within a pool since the time of entry.
\item {\it Transit time} is a random variable that describes the ages of the particles at the time they leave the boundaries of a system; i.e., the ages of the particles in the output flux.
\end{itemize}
}
%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
\frame{
\frametitle{Age and transit time are not always equal}

%\begin{multicols}{2}
%\includegraphics[scale=0.25]{Figures/Bolin}
%
%\vspace{2em} \tiny
%Bolin \& Rodhe (1973, Tellus 25: 58)
%
%\columnbreak

\small
\begin{itemize}
\item Mean transit time $>$ mean age: human population
\item Mean transit time $=$ mean age: Uranium, Radiocarbon
\item Mean transit time $<$ mean age: Ocean water
\end{itemize}

%\end{multicols}

}
%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Typical shape of a transit time distribution}
\centering
\includegraphics[scale=0.4]{Figures/transitTimeDistribution.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}[noframenumbering]
%\frametitle{Formulas for transit times and ages}
%For the autonomous system $\bm{\dot{x}}(t)=\tens{A}\,\bm{x}(t)+\bm{I}$, with $\bm{x^{\ast}} = - \mathbf{A}^{-1} \bm{I}$, and $\bm{z}^{T} = - \mathbf{1}^{T} \mathbf{A}$,
%
%\begin{itemize}
%\item {\it System age}
%	\begin{itemize}
%	\item $ f(a)= \bm{z}^T\,e^{a\,\tens{A}}\,\frac{\bm{x^\ast}}{\|\bm{x^\ast}\|}$
%	\item $ \E[a]=-\bm{1}^T\,\tens{A}^{-1}\,\frac{\bm{x^\ast}}{\|\bm{x^\ast}\|} = \frac{\|\tens{B}^{-1}\,\bm{x^\ast}\|}{\|\bm{x^\ast}\|} $
%	\end{itemize}
%\item {\it Pool age}
%	\begin{itemize}
%	\item $ \bm{f}(a) = (\tens{X^\ast})^{-1}\,e^{a\,\tens{A}}\,\bm{I} $
%	\item $ \E[\bm{a}] = -(\tens{X^\ast})^{-1}\,\tens{A}^{-1}\,\bm{x^\ast} $
%	\end{itemize}
%\item {\it Transit time}
%	\begin{itemize}
%	\item $f(\tau) = \bm{z}^T\,e^{\tau\,\tens{A}}\,\frac{\bm{I}}{\|\bm{I}\|}$
%	\item $ \E[\tau] = -\bm{1}^T\,\tens{A}^{-1}\,\frac{\bm{I}}{\|\bm{I}\|}=\frac{\|\bm{x^\ast}\|}{\|\bm{I}\|}$
%	\end{itemize}
%\end{itemize}
%
%\vspace{2em}
%\tiny Metzler \& Sierra (2018). Mathematical Geosciences 50: 1. 
%\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Three-pool model example}
\begin{multicols}{3}
\includegraphics[scale=0.2]{Figures/modelStructures}

\columnbreak

\begin{itemize} \tiny
\item One pool model: \\ $dx/dt = 100 - (1/16) x$
\item Three-pool model in parallel: \\ 
	$ \frac{d {\bm x}(t)}{dt} = \left( \begin{array}{c} 70 \\ 20 \\ 10 \end{array} \right) +
	\left( \begin{array}{ccc}
	-(1/4) & 0 & 0 \\
	0 & -(1/25) & 0 \\
	0 & 0 & -(1/100)
	\end{array} \right)
	\left( \begin{array}{c} x_1 \\ x_2 \\ x_3 \end{array} \right) $
\item Three-pool model in feedback: \\
	$ \frac{d {\bm x}(t)}{dt} = \left( \begin{array}{c} 70 \\ 30 \\ 0 \end{array} \right) +
	\left( \begin{array}{ccc}
	-(1/4) & 20/(55 \cdot 25) & 0 \\
	20/(90 \cdot 4) & -(1/25) & 1/100 \\
	0 & 5/(55 \cdot 25) & -(1/100)
	\end{array} \right)
	\left( \begin{array}{c} x_1 \\ x_2 \\ x_3 \end{array} \right) $
\end{itemize}
\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Three-pool model example}
\begin{multicols}{2}
\includegraphics[scale=0.2]{Figures/modelStructures}

\columnbreak

\begin{itemize} \tiny
\item One pool model:
	\begin{itemize} \tiny
	\item Turnover time: 16 yrs
	\item Mean Age: 16 yrs
	\item Mean transit time: 16 yrs
	\end{itemize}
\item Three-pool model in parallel:
	\begin{itemize} \tiny
	\item Turnover time: 16 yrs
	\item Mean Age: 63.8 yrs
	\item Mean transit time: 17.8 yrs
	\item Mean pool ages: 4, 25, 100 yrs.
	\end{itemize}
\item Three-pool model in feedback:
	\begin{itemize} \tiny
	\item Turnover time: 16 yrs
	\item Mean Age: 60.5 yrs
	\item Mean transit time: 22.35 yrs
	\item Mean pool ages: 13, 43, 143 yrs.
	\end{itemize}
\end{itemize}
\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Three-pool model example}
\begin{multicols}{2}
\includegraphics[scale=0.2]{Figures/modelStructures}

\columnbreak

\includegraphics[scale=0.35]{Figures/densities3p}
\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Three-pool model example}
%Pool age distributions: Parallel and feedback model structures
%\centering
%%\begin{multicols}{2}
%\includegraphics[scale=0.35]{Figures/parallel3p}
%%\columnbreak
%\includegraphics[scale=0.35]{Figures/feedback3p}
%%\end{multicols}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{A simple carbon cycle model}
\begin{multicols}{2}
\includegraphics[scale=0.23]{Figures/globalCmodel.pdf}

\columnbreak

\begin{equation}
{\bm x} = \left(  \begin{matrix}  % or pmatrix or bmatrix or Bmatrix or ...
      600  \\
      2300  \\
      900 
   \end{matrix} \right), 
   \quad
   \bm{I} = \left(
      \begin{matrix} % or pmatrix or bmatrix or Bmatrix or ...
         0 \\
         55 \\
         0 
      \end{matrix} \right)
    \notag
\end{equation} 

\begin{align}
\frac{d x_A}{dt} &= F_{AT} + F_{AS} - F_{TA} - F_{SA} \notag \\
\frac{d x_S}{dt} &= F_{SD} + F_{SA} - F_{DS} - F_{AS} \notag \\
\frac{d x_T}{dt} &= F_{TA} - F_{AT} \notag
\end{align}

%\begin{equation}
%\mathbf{F} =  \left(  \begin{matrix} % or pmatrix or bmatrix or Bmatrix or ...
%      -70 -120 & 70 & 120 \\
%      70 & -70-55 & 0 \\
%      120 & 0 & -120
%   \end{matrix} \right) \notag
%\end{equation}

\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{System of differential equations}
\begin{multicols}{2}
\includegraphics[scale=0.23]{Figures/globalCmodel.pdf}

\columnbreak

\begin{align}
\frac{d x_A}{dt} &= {x_T}{k_{AT}} + {x_S}{k_{AS}} - {x_A} ({k_{TA}} + {k_{SA}}) \notag \\
\frac{d x_S}{dt} &= I_S + {x_A}{k_{SA}} - {x_S} ({k_{DS}} + {k_{AS}}) \notag \\
\frac{d x_T}{dt} &= {x_A}{k_{TA}} - {x_T}{k_{AT}} \notag
\end{align}


\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{System of differential equations}
\begin{align}
\frac{d x_A}{dt} &= {x_T}{k_{AT}} + {x_S}{k_{AS}} - {x_A} ({k_{TA}} + {k_{SA}}) \notag \\
\frac{d x_S}{dt} &= I_S + {x_A}{k_{SA}} - {x_S} ({k_{DS}} + {k_{AS}}) \notag \\
\frac{d x_T}{dt} &= {x_A}{k_{TA}} - {x_T}{k_{AT}} \notag
\end{align}


\begin{equation}
\left( \begin{matrix}
    \frac{d x_A}{dt} \\ %&= {x_T}{k_{AT}} + {x_S}{k_{AS}} - {x_A} ({k_{TA}} + {k_{SA}}) \notag \\
    \frac{d x_S}{dt} \\ %&= I_S + {x_A}{k_{SA}} - {x_S} ({k_{DS}} + {k_{AS}}) \notag \\
    \frac{d x_T}{dt} \\ %&= {x_A}{k_{TA}} - {x_T}{k_{AT}} \notag
    \end{matrix} 
\right) = 
\left( \begin{matrix}
 0 \\ I_s \\ 0 
 \end{matrix}
\right) +
\left( \begin{matrix}
-(k_{TA}+k_{SA}) & k_{AS} & k_{AT} \\
k_{SA} & -(k_{DS} + k_{AS}) & 0 \\
k_{TA} & 0 & -k_{AT}
\end{matrix} \right) \cdot 
\left( \begin{matrix}
x_A \\ x_S \\ x_T
\end{matrix} \right)
\notag
\end{equation}

\begin{equation}
\frac{d {\bm x}}{dt} = {\bm I} + \mathbf{A} \cdot {\bm x} \notag
\end{equation}

}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Summary}
\begin{itemize}
%\item We use compartmental/reservoir/pool models to represent biogeochemical cycling
%\item Compartmental models are build using information about stocks and fluxes among reservoirs
%\item Models can be expressed in matrix form, explicitly representing connections among pools
\item Major model diagnostics are turnover time, system age, and transit time
\item For a single homogeneous pool at steady-state, all diagnostic times are equal
\item Ages and transit time change according to the connectivity of the system
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Final exam (Klausur)}
%
%\begin{itemize}
% \item Thursday July 6th, 2023, 8:00 -- 10:00 am. Burgweg 11.
% \item 20 questions. 100 points in total.
% \item Optional computer exercise: 22 points. 
%\end{itemize}
%
%}
%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
