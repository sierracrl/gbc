\documentclass{tufte-handout}

\title{The Global Carbon Cycle\thanks{Lecture notes for Global Biogeochemical Cycles, BBGW6.3.5, Friedrich Schiller University Jena, Summer Semester 2022}}

\author{Compiled by Carlos A. Sierra}

%\date{28 March 2010} % without \date command, current date is supplied

%\geometry{showframe} % display margins for debugging page layout

\usepackage{graphicx} % allow embedded images
  \setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}
  \graphicspath{{../../lecture2/}} % set of paths to search for images
\usepackage{amsmath}  % extended mathematics
\usepackage{booktabs} % book-quality tables
\usepackage{units}    % non-stacked fractions and better unit spacing
\usepackage{multicol} % multiple column layout facilities
\usepackage{lipsum}   % filler text
\usepackage{fancyvrb} % extended verbatim environments
  \fvset{fontsize=\normalsize}% default font size for fancy-verbatim environments

\usepackage{chemformula}

% Standardize command font styles and environments
%\newcommand{\doccmd}[1]{\texttt{\textbackslash#1}}% command name -- adds backslash automatically
%\newcommand{\docopt}[1]{\ensuremath{\langle}\textrm{\textit{#1}}\ensuremath{\rangle}}% optional command argument
%\newcommand{\docarg}[1]{\textrm{\textit{#1}}}% (required) command argument
%\newcommand{\docenv}[1]{\textsf{#1}}% environment name
%\newcommand{\docpkg}[1]{\texttt{#1}}% package name
%\newcommand{\doccls}[1]{\texttt{#1}}% document class name
%\newcommand{\docclsopt}[1]{\texttt{#1}}% document class option name
%\newenvironment{docspec}{\begin{quote}\noindent}{\end{quote}}% command specification environment

\begin{document}

\maketitle% this prints the handout title, author, and date

\begin{abstract}
\noindent
This document contains a compilation of pieces written by other authors on the global carbon cycle.
I extracted the most relevant information from different sources to give an accurate and updated overview of the global carbon cycle.
I also provide annotations to relevant sources to complement the reading material.
\end{abstract}

\section{The natural (without human perturbations) global carbon cycle}
\cite{Holmen2000}Although many elements are essential to living matter, carbon is the key element of life on Earth. The carbon atom's ability to form long covalent chains and rings is the foundation of organic chemistry and biochemistry. The biogeochemical cycle of carbon is necessarily very complex, since it includes all life forms on Earth as well as the inorganic carbon reservoirs and the links between them. Despite being a complicated elemental cycle, it is extensively studied and to date, probably the best understood elemental biogeochemical cycle. 

There are more than a million known carbon compounds, of which thousands are vital to life processes. The carbon atom's unique and characteristic ability to form long stable chains makes carbon-based life possible. Elemental carbon is found free in nature in three allotropic forms: amorphous carbon, graphite, and diamond. Graphite is a very soft material, whereas diamond is well known for its hardness. 
Carbon atoms have oxidation states ranging from +IV to -IV. The most common state is +IV in \ch{CO2} and the familiar carbonate forms. Carbonate exists in two reservoirs, in the oceans and as dissolved carbon in 
the forms of \ch{H2CO3}(aq), \ch{HCO3^-}(aq), and \ch{CO3^{2-}}(aq), and in the lithosphere as solid carbonate minerals: \ch{CaCO3}(s), \ch{CaMg(CO3)2}, \ch{FeCO3}. Carbon monoxide, CO, is a trace gas present in the atmosphere with carbon in oxidation state +II. Assimilation of carbon by photosynthesis creates the reduced carbon pools of the Earth. Reduced carbon is present with variable oxidation states that will be discussed further below. Methane, \ch{CH4}, is the most reduced form of carbon with an oxidation state of  -IV.

\cite{Stocker2003}For time scales not exceeding a few thousand years, the most important
reservoirs are the atmosphere, the ocean and the terrestrial biosphere (Fig. \ref{fig:naturalCcycle}).
Carbon appears in different forms in these reservoirs: in the atmosphere it is primarily 
\ch{CO2}, in the terrestrial biosphere in the form of fixed carbon in organic matter,
 whereas in the ocean the major part of the inventory is in the form of the 
 bicarbonate ion (\ch{HCO3}). The ocean inventory is over 60 times larger than the
atmospheric inventory and about 20 times larger than the terrestrial biosphere 
inventory and is thus the most important component in determining the level of 
atmospheric \ch{CO2}. Carbon is naturally cycled through these three reservoirs, and the
atmosphere serves as a gateway with a relatively short residence time of about
600 / (100 + 74) $\sim$ 3.5 years.\sidenote{Here, the author is assuming that the content of carbon in the atmosphere is constant, and the input and output fluxes are equal. This is the steady-state assumption, in which it is possible to compute a residence (turnover) time as a total stock divided by the total input or output flux. }
The exchange mechanisms between atmosphere and
terrestrial biosphere are photosynthesis (uptake of \ch{CO2} from the atmosphere) and
respiration (release of \ch{CO2} to the atmosphere), whereas gas exchange dominates
the fluxes between the ocean and the atmosphere.

Carbonate chemistry in the surface waters of the ocean is central to the 
understanding of the influence of and dynamics of changes in the ocean carbon content
upon atmospheric \ch{CO2}.

\begin{figure*}[h]
  \includegraphics[width=\linewidth]{naturalCcycle.pdf}%
  \caption{Natural global carbon cycle, with main reservoirs and fluxes that move carbon among reservoirs.}%
  \label{fig:naturalCcycle}%
\end{figure*}

Polar ice cores permit the reconstruction of past \ch{CO2} concentrations over at 
glacial cyles. Prior to the formation of ice, accumulated snow is
sintering into firn, a porous material which is in contact with atmospheric air. 
Upon closure of the air-filled spaces in the firn, the 
composition of the air remains trapped in the bubbles within the ice matrix. Provided
no further physical fractionation processes or chemical reactions occur, the air 
remains conserved in the bubbles. It depends on the location and the content of trace
materials (dust, organic matter) whether these conditions are sufficiently satisfied.
Once the ice core is retrieved from the ice sheets, the air is analyzed with various
methods to provide information on past concentration of trace gases in ancient air.
Carbon dioxide concentration is measured using a laser 
absorption technique.

Atmospheric \ch{CO2} concentrations vary in concert with the cycles of 
glaciation being around 270 to 290 ppmv during warm phases (interglacials) and
around 190 ppmv during full glacial times. The continuous rise over the last 250
years is clearly unprecedented (Fig. \ref{fig:iceCores}). Never in the last 420 000 years was the 
atmospheric \ch{CO2} concentration as high as it is now. 

\begin{figure*}[t]
  \includegraphics[width=\linewidth]{iceCores.JPG}%
  \caption{Atmospheric \ch{CO2} over the past 800,000 years as measured from bubbles in ice cores. The last part of the curve shows the rapid increase since the beginning of the industrial revolution. As reference points: Anatomically modern humans arose in Africa about 200,000 years ago. Reached behavioral modernity about 50,000 years ago. The Neolithic Revolution began 12,000 years ago. }%
  \label{fig:iceCores}%
\end{figure*}

\section{The global carbon cycle after the beginning of the industrial revolution}
\cite{Graven2016}The excess \ch{CO2} that’s accumulating in the atmosphere comes primarily from fossil-fuel combustion. Because carbon-14 has a half-life of 5700 years, fossil fuels, which are millions of years old, have lost all their \ch{^{14}C} to radioactive decay. Fossil- fuel combustion therefore decreases the ratio of radiocarbon to total carbon (\ch{^{14}C}/C) in the atmosphere. That isotopic change is passed on to plants through photosynthesis and is recorded in tree rings. In 1955 Hans Suess showed that the \ch{^{14}C}/C ratio in tree rings had decreased over the early 20th century. That decrease could have resulted only from the burning of fossil fuels. Soon after Suess’s discovery, Charles David Keeling began making high-precision measurements of atmospheric \ch{CO2} concentration. As shown in figure \ref{fig:mlo}, the long-term measurements that Keeling initiated unequivocally demonstrate the increasing \ch{CO2} concentration.

In 2015 Earth’s atmospheric \ch{CO2} concentration was 401 ppm, approximately 40\% higher than it was before the start of the Industrial Revolution circa 1870. Atmospheric \ch{CO2} concentration is now higher than at any time in at least the past several million years. And recent increases in atmospheric \ch{CO2}—the 2015 increase was 3.1 ppm—are much more rapid than at any time in the past 66 million years.\sidenote{You can see the trends in \ch{CO2} concentrations from Jena in this website \url{https://www.bgc-jena.mpg.de/~martin.heimann/weather/weather_co2/}}

\begin{figure*}[t]
  \includegraphics[width=\linewidth]{mlo_full_record_2022.pdf}%
  \caption{Atmospheric \ch{CO2} over the past 800,000 years as measured from bubbles in ice cores. The last part of the curve shows the rapid increase since the beginning of the industrial revolution. As reference points: Anatomically modern humans arose in Africa about 200,000 years ago. Reached behavioral modernity about 50,000 years ago. The Neolithic Revolution began 12,000 years ago. }%
  \label{fig:mlo}%
\end{figure*}

\sidenote{IPCC, 2021: Summary for Policymakers. In: Climate Change 2021: The Physical Science Basis. Contribution of Working Group I to the Sixth Assessment Report of the Intergovernmental Panel on Climate Change [Masson-Delmotte, V., P. Zhai, A. Pirani, S.L.
Connors, C. Péan, S. Berger, N. Caud, Y. Chen, L. Goldfarb, M.I. Gomis, M. Huang, K. Leitzell, E. Lonnoy, J.B.R. Matthews, T.K.
Maycock, T. Waterfield, O. Yelekçi, R. Yu, and B. Zhou (eds.)]. In Press.}
Observed increases in well-mixed greenhouse gas (GHG) concentrations since around 1750 are unequivocally caused
by human activities . Since 2011, concentrations have continued to increase in the
atmosphere, reaching annual averages of 410 parts per million (ppm) for carbon dioxide (CO 2 ). 
Land and ocean have taken up a near-constant
proportion (globally about 56\% per year) of \ch{CO2} emissions from human activities over the past six decades, with regional differences.

Each of the last four decades has been successively warmer than any decade that preceded it since 1850. Global
surface temperature in the first two decades of the 21st century (2001–2020) was 0.99 [0.84 to 1.10] $^{\circ}$C higher than
1850–1900. Global surface temperature was 1.09 [0.95 to 1.20] $^{\circ}$C higher in 2011–2020 than 1850–1900, with larger
increases over land (1.59 [1.34 to 1.83] $^{\circ}$C) than over the ocean (0.88 [0.68 to 1.01] $^{\circ}$C). 
It is likely that well-mixed GHGs contributed a warming of 1.0$^{\circ}$C to 2.0$^{\circ}$C, other
human drivers (principally aerosols) contributed a cooling of 0.0$^{\circ}$C to 0.8$^{\circ}$C, 
natural drivers changed global surface
temperature by –0.1$^{\circ}$C to +0.1$^{\circ}$C, and internal variability changed it 
by –0.2$^{\circ}$C to +0.2$^{\circ}$C. It is very likely that well-mixed
GHGs were the main driver 12 of tropospheric warming since 1979.

\section{The greenhouse effect}
\cite{Zhong2013}
The Earth is bathed in radiation from the Sun, which warms the planet and provides all the energy driving the climate system. Some of the solar (shortwave) radiation is reflected back to space by clouds and bright surfaces but much reaches the ground, which warms and emits heat radiation. This infrared (longwave) radiation, however, does not directly escape to space but is largely absorbed by gases and clouds in the atmosphere, which itself warms and emits heat radiation, both out to space and back to the surface. This enhances the solar warming of the Earth producing what has become known as the ‘greenhouse effect’. Global radiative equilibrium is established by the adjustment of atmospheric temperatures such that the flux of heat radiation leaving the planet equals the absorbed solar flux.

The role of different gases in the absorption and trapping of radiation in the atmosphere is illustrated in Figure \ref{fig:radiativeFlux}. This shows the spectrum of the radiative flux leaving the top of the atmosphere (TOA), calculated for a cloudless atmosphere, with global mean vertical profiles of temperature for \ch{H2O} and \ch{O3}, and the Earth's surface a black body at a temperature of 287.13K. Three other well-mixed greenhouse gases are included with concentrations for \ch{CO2} of 389 ppmv, \ch{CH4} 1.76 ppmv and \ch{N2O} 0.316 ppmv. Also in the figure coloured curves are shown, representing the spectra of radiation which would be emitted by black bodies at the temperatures given in the legend. The red curve is calculated at the surface temperature and if there were no atmosphere to interfere with the radiation then the emitted TOA spectrum would coincide with this. Clearly, however, the black curve falls below the red one at all wavelengths indicating that less radiation is emitted to space than leaves the surface. Dips in the curve indicate the wavelengths at which there is strong absorption by greenhouse gases of the radiation emanating from the ground and thus a greater contribution to the TOA flux from layers higher, and colder, in the atmosphere. The effective radiating temperature at each wavelength can be gauged by comparison with the blackbody curves at lower temperatures. The area under the black curve, being 257.7 Wm$^{-2}$, represents the total flux of longwave radiative energy leaving the planet. Spectra calculated in this way, using the correct atmospheric profiles, agree closely with satellite measurements of the infrared spectrum leaving the Earth, providing verification both for the radiative transfer theory and the spectral line database.

\begin{figure}[h]
  \includegraphics{radiativeFlux.pdf}%
  \caption{The black curve is a model-generated spectrum of the infrared radiative flux emitted to space at the top of the atmosphere (OLR). Coloured lines represent the blackbody spectrum at different temperatures (see legend). Regions of reduction in OLR due to the \ch{H2O} rotation bands (0-540cm$^{-1}$), \ch{CO2} 15$\mu$m band (550-800cm$^{-1}$), \ch{O3} 9.6$\mu$m band (980-1100cm$^{-1}$) and \ch{H2O} 6.3$\mu$m band (1400-1800cm$^{-1}$) are identified.}%
  \label{fig:radiativeFlux}
\end{figure}

\cite{Rodhe1990}The effect of 1 mol (or 1 kg) of \ch{CO2} in
the atmosphere depends to some extent on
the prevailing concentration of \ch{CO2}. 
Consider the emission of 1 kg of a gas into the atmosphere.
For most gases, it can be assumed that the
decay occurs exponentially with time (Fig.
\ref{fig:AGWP}), at a time constant (the half-life divided
by 0.7) equal to the turnover time of the
gas. For \ch{CO2}, a fraction disappears relatively
fast (within a few years), whereas the rest
decays much more slowly. The turnover
time of \ch{CO2} in the atmosphere is often
quoted as being 3 to 4 years only. Although
such a short time scale is a correct measure
of how rapidly CO2 is exchanged between
the atmosphere and the biota and the ocean
surface, respectively, it has no direct relation to the decay time of \ch{CO2} in the
atmosphere.

\begin{figure}[h]
  \includegraphics{Rodhe1.pdf}%
  \caption{Approximate decay history
for a unit release of various gases to
the atmosphere. The decay of \ch{CO2}
is taken from the model calculations.
The other curves are exponential
decays.}%
  \label{fig:AGWP}
\end{figure}

One way of comparing the emissions of
the gases from the point of view of the
greenhouse effect is to define the accumulated greenhouse effect as the integral of the
greenhouse effect over time. This concept
corresponds to dosage in radiology. Considering the emission of 1 kg of a certain gas i,
we can write
\begin{equation}
AG_i = \int_0^T k_i M_i(t) dt = k_i \int_0^T M_i(t) dt
\end{equation}
where $AG_i$ is the accumulated greenhouse
effect of an emission of 1 kg over the time
period $T$, $M_i(t)$ is the amount remaining in
the atmosphere after time $t$, and $k_i$ is the
greenhouse effect per kilogram in the atmosphere.

\bibliography{carbon-cycle-refs}
\bibliographystyle{plainnat}


\end{document}  