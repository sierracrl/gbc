\documentclass[aspectratio=169]{beamer}

 \usepackage{beamerthemesplit} %// Activate for custom appearance
 \usepackage{multicol}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage{graphicx}
\usepackage{chemformula}
\usepackage{pdfpages}

\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\deriv}[1]{\frac{d}{d#1}}
\newcommand{\intl}{\int\limits}
\def\tens#1{\ensuremath{\mathbf{#1}}}


%\usetheme{Luebeck}
\usetheme[compress]{MPIM}
 \usecolortheme{orchid}

\title[The Global S Cycle]{The Global Sulfur Cycle}
\author[Sierra, C.A.]{Carlos A. Sierra}
\institute{Max Planck Institute for Biogeochemistry}
\date{June 6, 2024}

\usefonttheme{serif}

\begin{document}

%%%%%%%%%%%%%%
\frame{\titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Assigned reading}
%\centering
%\includegraphics[page=1, scale=0.3]{articles/Shoonen2016.pdf}
%
%%Reading/lecture8-S-Kreislauf.pdf
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The Global S Cycle}
\centering
\includegraphics[scale=0.3]{Figures/Sulfur_Cycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Chemical forms of biogeochemical elements}
%\centering
%\includegraphics[scale=0.35]{../lecture7/Figures/chemicalForms}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Oxidation states of sulfur}
\centering
\includegraphics[scale=0.8]{Figures/OxidationStateS}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Geological and biological perspectives}
\centering
\begin{multicols}{2}
Geological perspective \\
\includegraphics[page=85, scale=0.65, trim={1cm, 19cm, 12cm, 3cm}, clip]{../../../Documents/Books/Fundamentals_of_Geobiology.pdf}

\columnbreak
Biological perspective \\
\includegraphics[page=86, scale=0.6, trim={10.5cm, 18cm, 3cm, 3.5cm}, clip]{../../../Documents/Books/Fundamentals_of_Geobiology.pdf}

\end{multicols} 
\tiny Canfield \& Farquhar (2021, Fundamentals of Geobiology)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Common forms of S in the Earth system}
\centering
\includegraphics[scale=0.38]{Figures/naturalCompounds}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global sulfur reservoirs}
\centering
\includegraphics[scale=0.45]{Figures/Sbudget}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur mineral forms}
\begin{multicols}{3}
Pyrite \ch{FeS2} \\
\includegraphics[scale=0.2]{Figures/pyrite}

\columnbreak
Gypsum \ch{CaSO4 2 H2O} \\
\includegraphics[scale=0.4]{Figures/gypsum} 

\columnbreak
Galena \ch{PbS} \\
\includegraphics[scale=0.11]{Figures/Galena}
\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur emissions from volcanic eruptions}
\centering
\includegraphics[scale=0.4]{Figures/AlaskaSemissions}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global volcanic emissions of \ch{SO2}}
\centering
\includegraphics[scale=0.6]{Figures/globalVolcanicEmissions}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur aerosols in the stratosphere (cloud formation, radiative effect)}
\centering
\includegraphics[scale=0.33]{Figures/volcanicEruptions}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Radiation effects of volcanic eruptions}
\centering
\includegraphics[scale=0.5]{Figures/RadiationEffects}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur and radiative forcing}
\centering
\includegraphics[scale=0.8]{Figures/RF_AR6.png} \\
\vspace{1em} \tiny IPCC 2021, AR6
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur in fossil fuels}
\begin{itemize}
\item Petroleum: 0.1 -- 10\%
\item Coal: 1 -- 20 \%
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Anthropogenic Sulfur Emissions}
\centering
\includegraphics[scale=0.5]{Figures/anthropogenicEmissions}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Acid rain}
\centering
\includegraphics[scale=0.3]{Figures/AcidRain2.jpg}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Acid rain effects}
\centering
\includegraphics[scale=0.203]{Figures/acidraineffects}
\includegraphics[scale=0.54]{Figures/acidraineffects2}
\includegraphics[scale=0.27]{Figures/acidraineffects3}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Reductions of ship tracks}
\centering
\includegraphics[scale=0.3]{Figures/EUemissions2021.png} \\
\vspace{2em} \tiny National emissions reported to the Convention on Long-range Transboundary Air Pollution (LRTAP Convention), European Environment Agency (EEA)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Reduction in low clouds due to ship fuel regulation}
\centering
\includegraphics[scale=0.28]{Figures/shiptracks_amo.jpg}
\includegraphics[scale=0.28]{Figures/shiptracks_anomaly.jpg} \\
\vspace{4em} \tiny Source: K. Mersmann 2022, NASA News
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Organic Sulfur}
Sulfur is essential for life. It is present in:
\begin{itemize}
\item Proteins
\item Sulfate esters of polysacharides
\item Steroids
\item Phenols
\item Enzymes
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Organic Sulfur}
Common organic sulfur molecules
\begin{itemize}
\item Methionine: \includegraphics[scale=0.4]{Figures/methionine}
\item Cysteine: \includegraphics[scale=0.3]{Figures/cysteine}
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur in organic cells}
\centering
\includegraphics[scale=0.38]{Figures/Scell}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur assimilation in plants}
\centering
\includegraphics[scale=0.4]{Figures/Sulfur_assimilation}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur in soils}
\centering
\includegraphics[scale=0.45]{Figures/TerrestrialSulfurCycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Distribution of sulfur in soils}
Organic sulfur (93\%)
\begin{itemize}
\item Carbon-bounded sulfur (cysteine and methionine) 41\%
\item Non-carbon-bonded sulfur (ester sulfates) 52\%
\end{itemize}

Inorganic sulfur (7\%)
\begin{itemize}
\item Adsorbed + soluble sulfates 6\%
\item Inorganic compounds less oxidized than sulfates and reduced sulfur compounds (e.g. sulfides) 1\%
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur fertilizer production and price}
\centering
\includegraphics[scale=0.15]{Figures/SulfurPrice}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur and soil micro-organisms}
\centering
\includegraphics[scale=1.2]{Figures/sulfursoil}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Anaerobic decomposition and sulfur}
%\centering
%\includegraphics[scale=0.25]{Figures/anaerobicDecomposition}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Reactants and products during anaerobic decomposition}
\centering
\includegraphics[scale=0.4]{Figures/wetlandDecomposition}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfate reduction in wetlands}
\centering
\includegraphics[scale=0.25]{Figures/sulfateReduction}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfate reducing bacteria}
\centering
\includegraphics[scale=0.25]{Figures/sulfateRespiration}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur emissions from aquatic systems}
\centering
\includegraphics[scale=0.3]{Figures/Semissions}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur species emitted from aquatic systems}
\centering
\includegraphics[scale=0.3]{Figures/gasSpecies}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Problems with hydrogen sulfide \ch{H2S}}
\begin{itemize}
\item Malodorous (rotten egg smell)
\item Acidic (corrosion/fouling)
\item Toxic 
\item Flamable
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbonyl sulfide (COS, OCS) in the atmosphere}
\centering
\includegraphics[scale=0.25]{Figures/ocs}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfide precipitation}
\centering
\includegraphics[scale=0.25]{Figures/sulfidePrecipitation}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Pyrite formation (framboids)}
\centering
\includegraphics[scale=0.2]{Figures/pyriteFormation}
\includegraphics[scale=0.23]{Figures/pyriteFramboid}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur cycle in wetlands}
\centering
\includegraphics[scale=0.25]{Figures/Swetlands}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur in the ocean}
\centering
\includegraphics[scale=0.3]{Figures/oceanScycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{DMS in the atmosphere and climate feedback?}
\centering
\includegraphics[scale=0.26]{Figures/DMSfeedback_new.jpg} \\
\vspace{1em} \tiny Charlson et al. (1987, Nature 326:655); Quinn \& Bates (2011, Nature 480: 51).
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Chemosynthesis in hydrothermal vents}
\centering
\includegraphics[scale=0.4]{Figures/chemosynthesis}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Diversity of sulfur-metabolizing microorganisms}
%\centering
%\includegraphics[scale=0.23]{Figures/Sphylogeny}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global S cycle}
%\centering
%\includegraphics[scale=0.3]{Figures/SKreislauf}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Solar radiation management}
\centering
\includegraphics[scale=1.2]{Figures/SRM.jpeg}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Solar radiation management}
\centering
\includegraphics[scale=0.9]{Figures/SRM2.jpeg}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Use of sulfur (sulfuric acid) in industry}
\begin{itemize}
\item Fertilizers
\item Paint manufacturing
\item Detergent manufacturing
\item Pesticide manufacturing
\item Electrolyte in batteries
\item Synthetic fibers
\item Cellophane manufacturing
\item Jet fuel
\item Paper bleaching
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Sulfur mining}
\centering
\includegraphics[scale=0.09]{Figures/sulfurMine.jpg}
\includegraphics[scale=0.0625]{Figures/sulfurMineIndonesia.jpg}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The sulfur cycle and its transit time}
\centering
\includegraphics[page=3, scale=0.8, trim={5.5cm 3cm 2cm 15.5cm}, clip]{../Reading/lecture8-S-Kreislauf.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Summary and take-home messages}
\begin{itemize}
\item Sulfur is a fundamental element for life. It take many different chemical forms in different compartments of the Earth system.
\item In the atmosphere, sulfur can reduce the amount of solar radiation reaching the Earth's surface, i.e. negative radiative forcing.
\item Sulfur plays a key role in acid rain and subsequent effects on ecosystems and infrastructure.
\item In wetlands and anoxic environments, sulfur is associated with complex metabolic pathways.
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}

