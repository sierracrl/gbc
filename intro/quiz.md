What is pollution?
- Atmospheric carbon dioxide
- Methane from arctic permafrost soils
- Human-induced change in the distribution of atoms from one place to another

The amount of matter transferred from one reservoir to another per unit time is:
- A reservoir
- A flux
- A budget
- A rate

