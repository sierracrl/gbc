# Globale Biogeochemische Stoffkreisläufe (BBGW 6.3.5)
Here you find all course material. In the lecture# folders, you find all slides and figures for all lectures.
In the reading folder, you find all assigned reading material for the course.

## Termine 2024
- 04.04. Intro to global biogeochemical cycles (Sierra)
- 11.04. The global carbon cycle I (Atmosphere and oceans, Sierra)
- 18.04. The global oxygen cycle (Hilman). 
- 25.04. Global carbon cycle II (terrestrial biosphere and methane, Hella van Asperen) 
- 02.05. TBD
- 09.05. Christi Himmelfahrt (Feiertag)
- 16.05. The global Nitrogen cycle (Sierra)
- 23.05. The global Phosphorous cycle (Sierra)
- 30.05. The global Sulphur cycle (Sierra)
- 06.06. Earth's energy balance and entropy production (Sierra)
- 13.06. TBD
- 20.06. TBD 
- 27.06. No class
- 11.07. Final exam
