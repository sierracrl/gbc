\documentclass[aspectratio=169]{beamer}

 \usepackage{beamerthemesplit} %// Activate for custom appearance
 \usepackage{multicol}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage{graphicx}
%\usepackage{minted}
\usepackage{chemformula}

\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\deriv}[1]{\frac{d}{d#1}}
\newcommand{\intl}{\int\limits}

%\usetheme{Luebeck}
\usetheme[compress]{MPIM}
 \usecolortheme{orchid}

\title[The global C cycle]{The Global Carbon Cycle}
\author[Sierra, C.A.]{Carlos A. Sierra}
\institute{Max Planck Institute for Biogeochemistry}
%\date{May 14, 2020}

\usefonttheme{serif}

\begin{document}

%%%%%%%%%%%%%%
\frame{\titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Outline}
\begin{itemize}
\item Introduction to the global C cycle, the greenhouse effect, and global carbon models
\item Modification to the global C cycle
%\item The global carbon budget and society
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Recommended reading}
\centering
\includegraphics[scale=0.2]{Graven2016} \\ \vspace{1em} \small
Graven, H.~D. (2016). The carbon cycle in a changing climate. {\em Physics Today}, 69(11):48--54.
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Pre-industrial carbon cycle}
\centering
\includegraphics[scale=0.35]{naturalCcycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Chemical forms of carbon}
%\centering
%\includegraphics[scale=0.35]{../lecture7/Figures/chemicalForms.pdf}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global C stocks}
\centering
\includegraphics[scale=0.35]{Cstocks}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The recent C cycle}
\centering
\includegraphics[scale=0.4]{CCycle} \\
\tiny Intergovernmental Panel on Climate Change (2013, 5th Assessment Report)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Atmospheric CO$_2$ over the past 800,000 years}
\includegraphics[scale=0.4]{iceCores} \\ \vspace{1cm}
\tiny Anatomically modern humans arose in Africa about 200,000 years ago. \\
Reached behavioral modernity about 50,000 years ago. \\
The Neolithic Revolution began 12,000 years ago. 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Recent increase of C dioxide in the atmosphere}
\centering
\includegraphics[scale=0.4]{maunaLoa} 
%\includegraphics[scale=0.28]{globalTemp} 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon dioxide and climate change}
\centering
\includegraphics[scale=0.28]{maunaLoa} 
\includegraphics[scale=0.28]{globalTemp} 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Contribution of GHG emissions to atmospheric warming}
\centering
\begin{multicols}{2}
\includegraphics[scale=0.25]{radiativeFlux.pdf}

\vspace{1em}
\tiny Zhong \& Haigh (2013, Weather 68: 100)

\columnbreak
\includegraphics[scale=0.3]{Rodhe1.pdf} \\
\includegraphics[scale=0.3]{Rodhe2.pdf} \\
\vspace{1em}
Rodhe (1990, Science 248: 1217)

\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Carbon emissions and global temperature change}
%\centering
%\includegraphics[scale=0.6]{TCRE} \\
%\tiny IPCC (2018)
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Seasonal cycle of atmospheric CO$_2$}
\centering
\includegraphics[scale=0.35]{seasonalCycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Earth's biosphere: oceanic and terrestrial components}
\centering
\includegraphics[scale=0.23]{global_biosphere} \\ \tiny \vspace{1em} {\it \textcopyright SeaWiFS project, NASA}
}
%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Atmospheric CO$_2$ in Jena}
%\centering
%\url{https://www.bgc-jena.mpg.de/~martin.heimann/weather/weather_co2/} \\ \vspace{2em}
%%Go to Weather Station $\to$ Time Series of CO$_2$ Concentration in Jena
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Atmospheric CO$_2$ and COVID-19}
%\centering
%\includegraphics[scale=0.16]{covid1}
%\includegraphics[scale=0.17]{covid2} \\
%\tiny \url{https://www.icos-cp.eu/event/933}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Atmospheric CO$_2$ records from other stations}
\begin{multicols}{2} 
\includegraphics[scale=0.3]{co2_stations}

\columnbreak
\includegraphics[scale=0.26]{co2_sta_records}
\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{3D animation of CO$_2$ transport}
\centering
\url{https://youtu.be/syU1rRCp7E8}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Seasonal cycle of atmospheric CO$_2$}
\centering
\includegraphics[scale=0.35]{Graven2016_1}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{What is the origin of carbon in the atmosphere?}
\emph{How do we know that the increase in atmospheric CO$_2$ is due to the combustion from fossil fuels?}

}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{isotopes}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{isotopeRatio}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{C13cycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.35]{SuessEffect13C}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{C14production}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{C14cycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.25]{SuessEffectC14}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.5]{bomb}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.9]{HuaSeries}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Carbon isotopes in the atmosphere}
\centering
\includegraphics[scale=0.6]{GravenPNAS}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Uptake of C from natural sinks}
\centering
%\includegraphics[scale=0.3]{gcp1}
\includegraphics[page=45, scale=0.4]{GCP2019.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Strength of natural sinks}
\centering
%\includegraphics[scale=0.32]{gcp2}
\includegraphics[page=47, scale=0.4]{GCP2019.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Possible reasons for the enhanced terrestrial C sink}
\centering
\includegraphics[scale=0.26]{regrowth}
}
%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Possible reasons for the enhanced terrestrial C sink}
\centering
\includegraphics[scale=0.25]{fertilization}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Possible trajectories predicted by ESMs}
\includegraphics[scale=1.1, trim={7cm 0 0 6cm}, clip]{Fried2} \\
\vspace{1em}
\tiny Friedlingstein et al. (2014)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Understanding processes in the terrestrial biosphere}
\centering
\includegraphics[scale=0.4]{transitTime.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Representation of ecosystem C processes in models}
%\centering
%\includegraphics[scale=0.5]{cModel}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The photosynthesis model of Farquhar et al.}
%\centering
%\includegraphics[scale=0.5]{Farquhar}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Environmental controls on photosynthesis}
%\centering
%\includegraphics[scale=0.45]{lightdose}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Allocation of carbon to different vegetation components}
%\centering
%\includegraphics[scale=0.8, trim={0 9cm 0 0}, clip]{Hartmann} \\
%\small A: assimilation, R: respiration, G; growth, NSC: non-structural carbohydrates
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Storage, cycling and release from soils}
%\centering
%\includegraphics[scale=0.4]{transitTime.pdf}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\centering
%\frametitle{Possible trajectories predicted by ESMs}
%\includegraphics[scale=1.1, trim={7cm 0 0 6cm}, clip]{Fried2} \\
%\vspace{1em}
%\tiny Friedlingstein et al. (2014)
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Future trajectories will depend on the balance between photosynthesis and respiration}
\centering
\includegraphics[scale=0.25, trim=20cm 15cm 0cm 0cm, clip]{GPP_Beer}
\includegraphics[scale=0.38]{Hashimoto} \\ 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=1,scale=0.55]{gcb2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=12, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=13, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=14, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=16, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=20, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=30, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{The global carbon budget 2019}
%\centering
%\includegraphics[page=35, scale=0.4]{GCP2019}
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%------------ Ocean C cycle
%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Strength of natural sinks}
\centering
\includegraphics[scale=0.32]{gcp2}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Marine carbon reservoirs}
\begin{itemize}
\item {\bf DIC}: Dissolved Inorganic C ($< 1 \mu$m) \; 38,100 Pg C
\item {\bf DOC}: Dissolved Organic C ($< 0.2 \mu$m) \; 662 Pg C
\item {\bf POC}: Particulate Organic C ($> 0.2 \mu$m) \; 25 Pg C
\item {\bf BC}: Black C ($< 0.2 \mu$m) \; $> 14$ Pg C
\item {\bf SOC}: Sedimentary Organic C ($0-1$ m) \; 150 Pg C
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Mechanisms for carbon ocean uptake and recycling}
\begin{itemize}
\item Abiotic inorganic cycling
\item Biotic carbon uptake and cycling
\end{itemize}

\centering
\includegraphics[scale=0.25]{../lecture4/bioticAbiotic}

}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Abiotic inorganic carbon cycling}
\centering
\includegraphics[scale=0.5]{../lecture4/airseaExchange} \\
\vspace{2em}
Sea-air gas exchange is mainly determined by wind speed and CO$_2$ solubility.
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Solubility of CO$_2$ is temperature dependent}
\centering
\includegraphics[scale=0.6]{../lecture4/solubility}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Equilibrium reactions in the ocean}
\centering
\ch{CO2} hydrates to \ch{H2CO3} \\
\vspace{2em}
\ch{CO2 + H2O <-> H2CO3} \\
\ch{H2CO3} (0.5\%) \ch{<-> H+ + HCO3-} (89\%) \\
\ch{HCO3- <-> H+ + CO3^2-} (10.5\%)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Equilibrium reactions in the ocean}
\centering
\includegraphics[scale=0.4]{../lecture4/pHRx}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Winds are a major driver of DIC transport in the surface ocean}
\centering
\includegraphics[scale=0.4]{../lecture4/winds}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Wind-driven transport}
\centering
\includegraphics[scale=0.4]{../lecture4/latitudinalTransport}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Surface currents and DIC transport}
\centering
\includegraphics[scale=0.5]{../lecture4/surfaceCurrents}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Thermohaline circulation (conveyor belt)}
\centering
\includegraphics[scale=0.5]{../lecture4/thermohaline}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Radiocarbon in the deep ocean}
\centering
\includegraphics[scale=0.6]{../lecture4/deepRadiocarbon}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Ocean acidification}
\centering
\includegraphics[scale=0.5]{../lecture4/acidification}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Biotic carbon cycling}
\centering
\includegraphics[scale=0.85]{../lecture4/bioticC}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Marine organic matter}
\centering
\includegraphics[scale=0.7]{../lecture4/mom}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Cycling of marine organic matter}
\centering
\includegraphics[scale=0.5]{../lecture4/momCycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{DOC concentrations globally}
\centering
\vspace{-2em}
\includegraphics[scale=0.4]{../lecture4/docStructure}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{DOC is much older than DIC}
\centering
\includegraphics[scale=0.38]{../lecture4/docRadiocarbon}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{DOC concentrations and carbon age}
\centering
\includegraphics[scale=0.5]{../lecture4/docAge}
}
%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Take home messages}
\begin{itemize}
\item The global carbon cycle has been largely modified by anthropogenic activities, particularly the atmospheric compartment
\item The seasonal cycle of atmospheric CO$_2$ is largely controlled by the biosphere
\item The land and the oceans take up together more than half of anthropogenic carbon emissions
\item Emissions of coal and oil from major global economies are responsible for most of the emissions
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Take-home messages: Ocean carbon cycle}
\begin{itemize}
\item Carbon cycling in the oceans is controlled by abiotic and biotic processes
\item Air-sea \ch{CO2} exchange is an important physical process responsible for the large reservoir of dissolved inorganic carbon (DIC)
\item Winds and currents mix DIC in the world's oceans
\item Additional \ch{CO2} in the atmosphere leads to ocean acidification
\item Dissolved organic carbon (DOC) is linked to biological processes and complex food-webs
\item DOC is very old!
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%


\end{document}


