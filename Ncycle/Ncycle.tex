\documentclass[aspectratio=169]{beamer}

 \usepackage{beamerthemesplit} %// Activate for custom appearance
 \usepackage{multicol}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage{graphicx}
\usepackage{chemformula}

\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\deriv}[1]{\frac{d}{d#1}}
\newcommand{\intl}{\int\limits}
\def\tens#1{\ensuremath{\mathbf{#1}}}


%\usetheme{Luebeck}
\usetheme[compress]{MPIM}
 \usecolortheme{orchid}

\title[The Global N Cycle]{The Global Nitrogen Cycle}
\author[Sierra, C.A.]{Carlos A. Sierra}
\institute{Max Planck Institute for Biogeochemistry}
\date{May 23, 2024}

\usefonttheme{serif}

\begin{document}

%%%%%%%%%%%%%%
\frame{\titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Recommended reading}
\centering
\includegraphics[scale=0.4, page=2]{../Reading/reactiveN_GallowayCowling.pdf}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The Global N cycle}
\centering
\includegraphics[scale=0.55]{Figures/globalNcycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global reservoir sizes}
\centering
\includegraphics[scale=0.25]{Figures/reservoirSize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Global reservoir sizes}
%\centering
%\includegraphics[scale=0.25]{Figures/reservoirs}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen gas (\ch{N2}) triple bond}
\centering
\includegraphics[scale=0.8]{Figures/nitrogen-triple-bond.jpg}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Forms (oxidation states) of nitrogen}
\centering
\includegraphics[scale=0.3]{Figures/oxidationStates}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{N forms and acronims}

\begin{itemize}
\item Nr: Reactive nitrogen
\item NOx: Nitrogen oxides, mainly nitric oxide (NO) and nitrogen dioxide (\ch{NO2})
\item NOz:  Nitrous acid (HONO), dinitrogen pentoxide(\ch{N2O5}), peroxyacetyl nitrate(PAN), alkyl nitrates (\ch{RONO2}), peroxyalkyl nitrates (\ch{ROONO2}), the nitrate radical (\ch{NO3}), and peroxynitric acid(\ch{HNO4})
\item NOy: NOx + NOz
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{N is essential for life}
N can be found in:
\begin{itemize}
\item Proteins: Collagen, Rubisco, enzymes
\item Nucleic acids: DNA, RNA
\item Energy nucleotides: ATP
\item Pigments: Chlorophylle
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen as encoder of information}
\centering
\includegraphics[scale=0.5]{Figures/RNADNA}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global nitrogen fixation}
\centering
\includegraphics[scale=0.5]{Figures/fixation}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Biological N fixation: BNF}
%Large amounts of energy are required to break \ch{N+N} bound
%\ch{N2}-fixing bacteria:
\centering
\includegraphics[scale=0.4]{Figures/Nfixers.pdf}

%\begin{itemize}
%\item Free living cyanobacteria
%\item Symbionts with fungi (lychens)
%\item Symbionts with higher plants (legumes, \emph{Alnus})
%\item Requires energy and electrons from organic matter
%\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Types of N fixers}
%\centering
%\includegraphics[scale=0.2]{Figures/typesNfixers}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{Types of N fixers}
%\begin{multicols}{2}
%\includegraphics[scale=0.35]{Figures/rhizobia}
%
%\columnbreak
%Symbiotic
%\begin{itemize}
%\item Legume-rhizobia
%\item Actinorhizal-Frankia
%\end{itemize}
%
%Associative
%\begin{itemize}
%\item Rhizosphere - Azospirillum
%\item Lichens - cyanobacteria
%\item Leaf nodules
%\end{itemize}
%\end{multicols}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global patterns of terrestrial BNF}
\centering
\includegraphics[scale=0.4]{Figures/terrestrialBNF}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Ocean BNF and external inputs (deposition \& river inflow)}
\centering
\includegraphics[scale=0.3]{Figures/oceanBNF}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen fixation by lightning}
Energy from lightning flashes can break the \ch{N2} triple bound, facilitating reaction with \ch{O2}

\ch{N2 + O2 ->[lightning] 2 NO} (nitric oxide) \\
It combines with oxygen to produce nitrogen dioxide \\
\ch{2 NO + O2 -> 2 NO2} \\
Nitrogen dioxide dissolves in water to produce nitric and nitrous acids \\
\ch{2 NO2 + H2O -> HNO3 + HNO2}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Lightning flashes seen from space (2000)}
\centering
\includegraphics[scale=0.25]{Figures/flashes} \\
Emission = (\# flashes) x (\ch{HNOx} molecules/flash)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The Haber-Bosch Process}
\begin{itemize}
\item Artificial process for nitrogen fixation
\item Developed by Fritz Haber and Carl Bosch in the early 1920s
\item Main method to produce N fertilizer
\item It converts atmospheric nitrogen (\ch{N2} ) to ammonia (\ch{NH3}) by a reaction with hydrogen (\ch{H2}) under high temperature and pressure \\
\ch{N2 + 3 H2 -> 2 NH3}
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The Haber-Bosch Process}
\centering
\includegraphics[scale=0.18]{Figures/HaberBosch}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen inputs and outputs in croplands}
\centering
\includegraphics[scale=0.4]{Figures/Ncropland} \\
136.6 Tg N enter croplands per year \\
$\sim$50\% of N inputs are from N fertilizers \\
55\% of inputs are taken up by harvest and residues
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen deposition from dry and wet sources}
\centering
\includegraphics[scale=0.5]{Figures/Ndeposition}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The cycle of reactive nitrogen}
\centering
\includegraphics[scale=0.3]{Figures/Ncycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen in terrestrial ecosystems}
\centering
\includegraphics[scale=0.25]{Figures/ecosystemN}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen in wetland soils}
\centering
\includegraphics[scale=0.25]{Figures/soilN}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{\ch{N2O} emissions (GWP = 298)}
\centering
\includegraphics[scale=1]{Figures/N2O}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen leaching from ecosystems (25-35 TgN/yr)}
\centering
\includegraphics[scale=0.27]{Figures/Noutput}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Denitrification and N transport}
\centering
\includegraphics[scale=0.3]{Figures/denitrificationStream}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen cycling in the oceans}
\centering
\includegraphics[scale=0.2]{Figures/oceanN}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen and carbon fixation in the ocean}
\centering
\includegraphics[scale=0.27]{Figures/oceanBNFmechanisms}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Ocean BNF and nutrient co-limitation}
\centering
\includegraphics[scale=0.27]{Figures/oceanBNFcolimitation}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Balancing N sources and sinks}
\centering
\includegraphics[scale=0.5]{Figures/sourcesSinks}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Reactive nitrogen in the atmosphere}
\centering
\includegraphics[scale=0.35]{Figures/Natmosphere}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen sources to the atmosphere}
\centering
\includegraphics[scale=0.25]{Figures/Nsources}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\frametitle{\ch{N2O} and other greenhouse gases}
%\centering
%\includegraphics[scale=0.125]{Figures/ghgs.pdf}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{NOx and Ozone}
\centering
\includegraphics[scale=0.27]{Figures/NOxO3}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Atmospheric ozone}
\centering
\includegraphics[scale=0.5]{Figures/Atmospheric_ozone}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Ozone pollution}
\centering
\includegraphics[scale=0.7]{Figures/ozone-pollution}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Global scale production of reactive N}
\centering
\includegraphics[page=4, trim={3.5cm, 4cm, 2cm, 12cm}, clip, scale=1]{Figures/Galloway2021.pdf} \\
\tiny Galloway et al. (2021, Annual Review of Environment and Resources 46: 255)
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Reactive nitrogen and global biogeochemical cycles}
\centering
\includegraphics[scale=0.27]{Figures/Nr_GBC}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The nitrogen cascade}
\centering
\includegraphics[scale=0.2]{Figures/Ncascade}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Humans and reactive nitrogen}
\centering
\includegraphics[scale=0.2]{Figures/humansNr} \\
The human body requires $\sim 2$ kg N yr$^{-1}$ of protein to survive. \\
In the 1990s, humans created about 110 Tg N yr$^{-1}$, but needed only 11 Tg. \\
What happens to the 100 Tg N yr$^{-1}$ that is produced but not needed?
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen efficiency and diet}
\centering
\includegraphics[scale=0.4]{Figures/Ndiet}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Nitrogen in international trade}
\centering
\includegraphics[scale=0.19]{Figures/Ntrade}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Soy fields, deforestation, and water-energy balances}
\centering
\includegraphics[scale=0.5]{Figures/soyField.jpg}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Summary and conclusions}
\begin{itemize}
\item The global nitrogen cycle is one of the most altered by human activities
\item Reactive nitrogen exists in many different forms and can go through many reactions
\item Reactive nitrogen goes through many transformation in terrestrial and aquatic systems, with environmental impacts in atmosphere, land and oceans
\item Through denitrification, reactive nitrogen can go back to inert form (\ch{N2})
\item Less awareness in the general public on the modification of the N cycle than on the C cycle
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
